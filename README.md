## filter_palindromes.py

This repo contains a simple script that identifies palindromic sequencing reads using minimap2. Basically, we identify them by attempting to align each read to its reverse compliment. It will then write non palindromic reads to stdout, optionally write palindromic reads to a specified file location, and outputs simple stats on stderr. For full usage, use either "filter_palindromes.py -h" or "filter_palindromes.py --help"

## Example Usage


```
# Filters the reads in some.fastq and writes the passing reads to good_reads.fastq
filter_palindromes.py some.fastq > good_reads.fastq

# Unzips a gziped fastq file and passes it to filter_palindromes
gunzip -c some.fastq.gz | filter_palindromes.py > good_reads.fastq

# Pattern for zipped input when you want to capture the stats that are output and the failing reads while zipping the good reads
gunzip -c some.fastq.gz | filter_palindromes --palindromes fails.fastq 2> some.stats | gzip > good_reads.fastq.gz
```

