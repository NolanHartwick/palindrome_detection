#!/usr/bin/env python3

import mappy
import argparse
import sys
import time
import multiprocessing
import itertools
import os


def iter_group(iterable, chunk, fillvalue=None):
    """ Iterate over iterable returning chunk number of elements at a time

        iter_group(fq_file, 4) makes for a passable fastq parser
    """
    iters = [iter(iterable) for i in range(chunk)]
    for c in itertools.zip_longest(*iters, fillvalue=fillvalue):
        yield c


def test_read(r, frac=0.75):
    """ Align a read to its reverse compliment. If the read is a "palindrome",
        it should map succesfully and will fail otherwise.

        retuns true if read is NOT palindromic
    """
    # 0x100000 means forward only alingment
    a = mappy.Aligner(seq=r, preset="ava-ont", extra_flags=0x100000)
    for hit in a.map(mappy.revcomp(r)):
        if(abs(hit.q_st - hit.q_en) > frac * len(r)):
            return False
    return True


def test_fq_entries(fq_chunk, frac=0.75):
    """ Test each read in this chunk of the fastq
    """
    return [test_read(entry[1], frac) for entry in fq_chunk]


def filter_palindromic_reads(fq, fraction, processes, chunksize, passfile, failfile):
    """ Reads fastq data from fq, filters out palindromic reads, then
        writes non-palindromic reads to outfile. Returns a tuple containing
        the count of non-palindromic reads and the count of palindromic reads

        Input:
            fq : a readable file object containing text represneting fastq
            fraction : how long the revcomp alignment for palindromes must be
            processes : number of processes to use
            chunksize : how many fastq entries per processes to load
            passfile : location where non palindromic reads should be written
            failfile : location where palindromic reads should be written

        Returns: (int, int)
            a tuple containing the count of non-palindromic reads and the
            count of palindromic reads, respectively
    """
    # multiprocessing.set_start_method("spawn")
    with multiprocessing.Pool(processes) as p:
        fq_iter = iter_group(fq, 4)
        chunk_iter = iter_group(fq_iter, chunksize)
        block_iter = iter_group(chunk_iter, processes)
        good_count = 0
        fail_count = 0
        for block in block_iter:
            # filter nulls from block
            block = [
                [entry for entry in chunk if(entry is not None)]
                for chunk in block if(chunk is not None)
            ]
            block_result = p.map(test_fq_entries, block)
            for chunk, chunk_res in zip(block, block_result):
                for entry, entry_res in zip(chunk, chunk_res):
                    if(entry_res):
                        good_count += 1
                        passfile.write("".join(entry))
                    else:
                        fail_count += 1
                        failfile.write("".join(entry))

    return (good_count, fail_count)


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "Filters palindromic reads from a fastq file. Non-palindromic "
            "reads will be written to stdout. Memory usage is roughly "
            "proportional to processes * chunksize * average_read_length. "
            "Requires Minimap2, Mappy, and python 3.6+."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "fq",
        type=argparse.FileType('r'),
        nargs="?",
        default=sys.stdin,
        help="a fastq file"
    )
    parser.add_argument(
        "-f", "--fraction",
        default=0.75,
        type=float,
        help=(
            "How large the alignment for a read to its reverse compliment "
            "relative to that reads length needs to be for the read to be "
            "called a palindrome."
        )
    )
    parser.add_argument(
        "-p", "--processes",
        default=8,
        type=int,
        help="Number of processes to use."
    )
    parser.add_argument(
        "-c", "--chunksize",
        default=1000,
        type=int,
        help=(
            "How many reads should be handled by each process at a time. "
            "Smaller values consume less memory but run less efficiently."
        )
    )
    parser.add_argument(
        "--palindromes",
        default=open(os.devnull, "w"),
        type=argparse.FileType('w'),
        help=(
            "A location where palindromic reads should be written. Default "
            "behavior is to NOT write palindromic reads anywhere."
        )
    )
    args = parser.parse_args()
    return args


def main():
    """ Iterates over a fastq file generating blocks of chunks of reads
        Each chunk gets processed in parrelel before moving onto the next
        block. Each fastq entry in each chunk that is not palindromic will
        be written to stdout. Some info will then be printed to stderr
    """
    args = get_args()
    start = time.time()
    good_count, fail_count = filter_palindromic_reads(
        args.fq,
        args.fraction,
        args.processes,
        args.chunksize,
        sys.stdout,
        args.palindromes
    )
    sys.stderr.write(f"Count of Good Reads : {good_count}\n")
    sys.stderr.write(f"Count of Bad Reads : {fail_count}\n")
    sys.stderr.write(f"Fraction Palindromic : {fail_count / (good_count + fail_count)}\n")
    sys.stderr.write(f"Run Time : {time.time() - start}s\n")
    sys.stderr.write("\n")


if(__name__ == "__main__"):
    main()
